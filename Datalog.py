from Graph import Graph
from Tools import deleteSpaces, conditionParser, respectCondition


class Datalog:

    def __init__(self, filename="datalog.dl"):
        self.file = open(filename, "r")

        self.EDB = dict()
        self.IDB = dict()

    def read(self):
        lines = self.file.readlines()

        for line in lines:

            if line=='\n' or line[0]=='#':
                continue

            if line.find(":-")==-1:
                self.parseEDB(line)
            else :
                self.parseIDB(line)


    def parseIDB(self, line):
        headBodySeparator = line.find(":-")

        # ===== Parsing de la tete de la requête =====
        firstPart = line[:headBodySeparator]

        headName = firstPart[:firstPart.find('(')]
        headParam = firstPart[firstPart.find('(')+1:firstPart.find(')')].split(',')
        headParam = deleteSpaces(headParam)

        self.IDB[headName] = []

        # ====== Parsing du corps de la requête ======
        secPart   = line[headBodySeparator+2:]


        start = 1
        subQueries = []

        aggregateFunction = None


        # get all subQueries
        while True :
            end = secPart[start:].find(')')

            # if no other subQueries => its conditions (IC)
            if end == -1:
                conditionalArg = conditionParser(secPart[start+1:])
                break

            end += start
            sub = secPart[start:end+1]

            if sub[0] == ',':
                sub = sub[1:]

            sub = sub.strip()

            name = sub[:sub.find('(')]
            params = sub[sub.find('(') + 1:sub.find(')')].split(',')
            params = deleteSpaces(params)

            if name=='COUNT' or name=='SUM' or name=="AVG" or name=="MAX" or name=="MIN":
                table = None
                groupBy = None
                varAggregate = None
                for s in subQueries:
                    for i in range(len(s['params'])):
                        if params[0] == s['params'][i]:
                            table = s['name']
                            groupBy = i
                        if params[1] == s['params'][i]:
                            varAggregate = i

                aggregateFunction = {'table' : table, 'groupBy':groupBy, 'varAggregate':varAggregate ,'resultVar': params[2], 'type':name}


            else:
                subQueries.append({"name":name, "params":params})

            start = end+1

        # init dependencyGraph from IDB parsing values
        dependencyGraph = Graph(subQueries, headName, headParam, aggregateFunction)


        # Evaluate the query
        self.evaluateQuery(dependencyGraph, headName, headParam, subQueries, conditionalArg, aggregateFunction)




    def evaluateQuery(self, dependencyGraph, headName, headParam, subQueries, conditionalArgs, aggregateFunction):


        recursiveQuery = False
        for e in dependencyGraph.E:
            if not e[4]:
                if e[0] == headName or e[1] == headName:
                    recursiveQuery = True

        while True:
            join = dict()

            for e in dependencyGraph.E:
                if not e[4]:
                    tableName1 = e[0]
                    tableName2 = e[1]

                    sub1 = None
                    sub2 = None

                    for subQ in subQueries:
                        if sub1 is None and subQ['name'] == tableName1:
                            sub1 = subQ
                        if subQ['name'] == tableName2 and subQ != sub1:
                            sub2 = subQ


                    join = self.join(table1=tableName1,
                                     table2=tableName2,
                                     param_table1=e[2],
                                     param_table2=e[3],
                                     join_table=join,
                                     condition=conditionalArgs,
                                     sub1=sub1,
                                     sub2=sub2)


            # when only one table in query body so no join
            if len(join)==0:
                tableName1 = subQueries[0]['name']
                sub1 = None
                for subQ in subQueries:
                    if subQ['name'] == tableName1:
                        sub1 = subQ


                join = self.join(table1=tableName1,
                                 table2=None,
                                 param_table1=None,
                                 param_table2=None,
                                 join_table=join,
                                 condition=conditionalArgs,
                                 sub1=sub1,
                                 sub2=None)

            self.constructResult(headName, headParam, join, len(join[subQueries[0]['name']]), dependencyGraph, aggregateFunction)


            newValue = True
            if headName in self.EDB:
                for idb in self.IDB[headName]:
                    if not idb in self.EDB[headName]:
                        newValue = False
            else :

                self.EDB[headName] = []

            # if no changes => break loop
            if not recursiveQuery or newValue:# or self.IDB[headName] == self.EDB[headName] or len(self.IDB[headName])==0:
                self.addIDBToEDB(headName)
                break

            self.addIDBToEDB(headName)


    def addIDBToEDB(self, headName):
        for idb in self.IDB[headName]:
            if not idb in self.EDB[headName]:
                self.EDB[headName].append(idb)


    def constructResult(self, headName, headParam, join, size, dependencyGraph, aggregateFunction):
        res = []

        if aggregateFunction is None:
            for i in range(size):
                tmp = [None] * len(headParam)

                for e in dependencyGraph.E:

                    if e[4]:
                        tableName = e[1]

                        id = join[tableName][i]
                        param = e[3]
                        tmp[e[2]] = self.EDB[tableName][id][param]


                res.append(tmp)
        else :
            tableAggregate = aggregateFunction['table']
            typeAggregate  = aggregateFunction['type']
            groupBy        = aggregateFunction['groupBy']
            varAggregate   = aggregateFunction['varAggregate']
            category = list()

            for j in join[tableAggregate]:
                category.append(self.EDB[tableAggregate][j][groupBy])

            distinctValues = list(set(category))



            # ===== COUNT =====
            if typeAggregate == "COUNT":
                join['AGGREGATE'] = [category.count(v) for v in distinctValues]



            # ===== SUM =====
            elif typeAggregate == "SUM":
                join['AGGREGATE'] = [0 for v in distinctValues]

                for i in range(len(distinctValues)):
                    for j in join[tableAggregate]:
                        if self.EDB[tableAggregate][j][groupBy] == distinctValues[i]:
                            join['AGGREGATE'][i] += int(self.EDB[tableAggregate][j][varAggregate])

            # ===== AVG =====
            elif typeAggregate == "AVG":
                join['AGGREGATE'] = [0 for v in distinctValues]
                cmpt = [0 for v in distinctValues]

                for i in range(len(distinctValues)):
                    for j in join[tableAggregate]:
                        if self.EDB[tableAggregate][j][groupBy] == distinctValues[i]:
                            join['AGGREGATE'][i] += int(self.EDB[tableAggregate][j][varAggregate])
                            cmpt[i] += 1


                for i in range(len(join['AGGREGATE'])):
                    join['AGGREGATE'][i] /= cmpt[i]
                    join['AGGREGATE'][i] = round(join['AGGREGATE'][i], 3)

            # === MAX ===
            elif typeAggregate == "MAX":
                join['AGGREGATE'] = [None for v in distinctValues]
                for i in range(len(distinctValues)):
                    for j in join[tableAggregate]:
                        if self.EDB[tableAggregate][j][groupBy] == distinctValues[i]:
                            if join['AGGREGATE'][i] is None or join['AGGREGATE'][i] < self.EDB[tableAggregate][j][varAggregate]:
                                join['AGGREGATE'][i] = self.EDB[tableAggregate][j][varAggregate]

            # === MIN ===
            elif typeAggregate == "MIN":
                join['AGGREGATE'] = [None for v in distinctValues]
                for i in range(len(distinctValues)):
                    for j in join[tableAggregate]:
                        if self.EDB[tableAggregate][j][groupBy] == distinctValues[i]:
                            if join['AGGREGATE'][i] is None or join['AGGREGATE'][i] > self.EDB[tableAggregate][j][
                                varAggregate]:
                                join['AGGREGATE'][i] = self.EDB[tableAggregate][j][varAggregate]




            for i in range(len(distinctValues)):
                tmp = [None] * len(headParam)
                for e in dependencyGraph.E:
                    if e[4]:
                        tableName = e[1]
                        if tableName=="AGGREGATE":
                            tmp[e[2]] = str(join[tableName][i])
                        else :
                            tmp[e[2]] = distinctValues[i]

                res.append(tmp)


        if headName in self.EDB:

            for r in res:
                if not r in self.IDB[headName]:
                    self.IDB[headName].append(r)

        else:
            self.IDB[headName] = res




    def join(self, table1, table2, param_table1, param_table2, join_table, condition, sub1, sub2):


        if len(join_table)==0:

            join_table[table1] = list()

            for i in range(len(self.EDB[table1])):
                if respectCondition(condition, sub1['params'], self.EDB[table1][i]):
                    join_table[table1].append(i)

        # when only one table in query body
        if table2 is None and param_table2 is None and sub2 is None:
            return join_table

        join_table[table2] = list()
        new_table1 = list()
        deleted_id_table1 = list()

        for id in range(len(join_table[table1])):
            find = False
            for j in range(len(self.EDB[table2])):
                i = join_table[table1][id]
                if self.EDB[table1][i][param_table1] == self.EDB[table2][j][param_table2]:
                    if respectCondition(condition, sub2['params'], self.EDB[table1][i]):
                        find = True
                        new_table1.append(i)
                        join_table[table2].append(j)
            if not find:
                deleted_id_table1.append(id)

        # delete
        for key in join_table:
            if key != table2 and key != table1:
                for d in deleted_id_table1:
                    join_table[key].pop(d)

        join_table[table1] = new_table1


        return join_table

    def parseEDB(self, line):
        pOpen = line.find('(')

        atome = line[0:pOpen]

        attribute = line[pOpen + 1:].split(',')

        attribute[-1] = attribute[-1][0:attribute[-1].find(')')]

        for i in range(len(attribute)):
            if attribute[i][0] == '\'' and attribute[i][-1] == '\'':
                attribute[i] = attribute[i][1:-1]

        if atome in self.EDB:
            self.EDB[atome].append(attribute)
        else:
            self.EDB[atome] = list([attribute])
