def testCondition(operator, valueNeeded, valueTested):
    if operator=='=':
        return valueTested == valueNeeded
    elif operator=='<':
        return int(valueTested) < int(valueNeeded)
    elif operator=='>':
        return int(valueTested) > int(valueNeeded)
    elif operator=='!=':
        return int(valueTested) != int(valueNeeded)

    return False

def display_dict(d):

    COLOR_GREEN = '\033[32m'
    RESET_COLOR = '\033[0m'


    for key, value in d.items():
        if len(value)==0:
            print(RESET_COLOR + key + " : No results\n")
            continue

        #value_widths = [ max(len(value[j][i]) for j in range(len(value))) + 2 for i in range(len(value[0]))]
        value_widths = [max(len(value[j][i]) if (value[j][i] is not None and len(value[j][i]) > 0) else 0 for j in range(len(value))) + 2 for i in range(len(value[0]))]

        width = sum(value_widths)
        nb_space = (width-len(key))//2
        print(RESET_COLOR+" "*nb_space+key+" "*nb_space)


        for i in range(len(value)):

            for v in value_widths:
                print(COLOR_GREEN+"+"+"-"*v, end='')
            print(COLOR_GREEN+'+')

            for j in range(len(value[i])):
                if value[i][j] is None:
                    print(COLOR_GREEN + "| ", end='')
                    print(RESET_COLOR + " " * (value_widths[j]-1), end='')
                else :
                    dif = value_widths[j] - len(value[i][j]) -1
                    print(COLOR_GREEN+"| ", end='')
                    print(RESET_COLOR+value[i][j]+" "*dif, end='')
            print(COLOR_GREEN + "|")

        for v in value_widths:
            print(COLOR_GREEN+"+" + "-" * v, end='')
        print(COLOR_GREEN+'+')
        print(RESET_COLOR)


def deleteSpaces(stringList):
    for i in range(len(stringList)):
        stringList[i] = stringList[i].replace(" ", "")

    return stringList



def conditionParser(line):
    lineSplit = line.split(',')

    if len(lineSplit)==0 or lineSplit[0]=='\n':
        return dict()

    result = dict()

    for tmp in lineSplit:
        tmp = tmp.replace(".", "")
        tmp = tmp.replace("\n", "")
        tmp = tmp.replace(" ", "")

        operator = None

        if tmp.find('=') != -1:
            if tmp.find('!=') != -1:
                operator = '!='
                tmp = tmp.split('!=')
            else :
                tmp = tmp.split('=')
                operator = '='
        elif tmp.find('<') != -1:
            tmp = tmp.split('<')
            operator = '<'
        elif tmp.find('>') != -1:
            tmp = tmp.split('>')
            operator = '>'
        if not operator is None:
            result[tmp[0]] = {'value': tmp[1], 'operator': operator}

    return result


def respectCondition(conditions, subQueryParams, valueTested):
    for p in range(len(subQueryParams)):
        for key, value in conditions.items():
            if subQueryParams[p] == key:

                if not testCondition(operator=value['operator'],
                                     valueNeeded=value['value'],
                                     valueTested=valueTested[p]):
                    return False

    return True