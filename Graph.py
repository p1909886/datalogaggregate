class Graph:
    def __init__(self, subQueries, headName, headParam, aggregateFunctions):
        self.V = list()
        self.E = list()


        for sub in subQueries:
            self.add_V(sub["name"])

        self.add_V(headName)


        for i in range(len(subQueries)):
            for j in range(i+1, len(subQueries)):
                for p1 in range(len(subQueries[i]["params"])):
                    for p2 in range(len(subQueries[j]["params"])):
                        if subQueries[i]["params"][p1] != "_" and subQueries[i]["params"][p1] == subQueries[j]["params"][p2]:
                            self.add_E((subQueries[i]["name"], subQueries[j]["name"], p1, p2, False))

            for p1 in range(len(headParam)):
                for p2 in range(len(subQueries[i]['params'])):
                    if subQueries[i]['params'][p2] != "_" and headParam[p1] == subQueries[i]["params"][p2]:
                        self.add_E((headName, subQueries[i]["name"], p1, p2, True))

        if not aggregateFunctions is None:
            self.add_V('AGGREGATE')

            paramHead = 0
            for i in range(len(headParam)):
                if aggregateFunctions['resultVar'] == headParam[i]:
                    paramHead = i

            self.add_E((headName, "AGGREGATE", paramHead, 0, True))


    def add_V(self, vertices):
        if isinstance(vertices, list):
            for v in vertices:
                self.V.append(v)
        else :
            self.V.append(vertices)

    # edge is a tulpe (vertex1, vertex2, commonParamv1, commonParamv2, head)
    def add_E(self, edge):
        if edge[0] in self.V and edge[1] in self.V:
           self.E.append(edge)
        else :
            print("A vertex doesn't exists")

    def get_neighbours(self, idV):
        res = list()
        for e in self.E:
            if e[0] == idV:
                res.append((e[1], e[2], e[3]))

        return res

    def print(self):
        print("Vertices :", self.V)
        print("Edges :", self.E)